# IPv6 resources

## Books

* [https://github.com/becarpenter/book6](https://github.com/becarpenter/book6)

## Cloud

* [https://github.com/DuckbillGroup/aws-ipv6-gaps/](https://github.com/becarpenter/book6)

## Certification
* [Hurricane Electric](https://ipv6.he.net/certification/)

## Statistics

* [Akamai](https://www.akamai.com/de/internet-station/cyber-attacks/state-of-the-internet-report/ipv6-adoption-visualization)
* [Cisco 6Lab](https://6lab.cisco.com)
* [Cloudflare Radar](https://radar.cloudflare.com/reports/ipv6)
* [Facebook](https://www.facebook.com/ipv6)
* [Google IPv6](https://www.google.com/ipv6)
* [Potaroo BGP](https://bgp.potaroo.net/index-v6.html)

## Tools

### CLI

* [sipcalc](https://github.com/sii/sipcalc)
* [aggregate6](https://github.com/job/aggregate6)
* [grepcidr](https://github.com/ryantig/grepcidr)

## Videos

* [Klara Mall - IPv6 am KIT: Erfahrungsbericht und aktueller Status (German)](https://opencast.hu-berlin.de/paella/ui/watch.html?id=2fdf575e-eec7-4131-88fa-4b8f39c0e6d6)

## Webinars

* [RIPE IPv6 Webinars](https://www.ripe.net/support/training/material/webinar-slides/ipv6security-2hrs-part3.pdf)
