# IPv6 Resources

This repository provides a list to all kinds of IPv6 related resources,
from interesting websites and blog posts to videos and webinars.

I had the idea after sharing several links with some people in an IPv6 project
I'm working on.

## How to contribute

* create an Issue
* create a merge / pull request (prefred)
* mark commerical products, like books, training and tools
* mark links thar are not in English

## Why gitlab and not the other provider?

```
$ dig aaaa gitlab.com +short
2606:4700:90:0:f22e:fbec:5bed:a9b9
```
